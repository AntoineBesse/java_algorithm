package dii.vrp.tp;

public class Saving  implements Comparable<Saving>{
	/**
	 * The id of the node
	 */
	private final int neighborID_1;
	/**
	 * The id of the neighbor node
	 */
	private final int neighborID_2;
	/**
	 * The distance between <code>id</code> and <code>neighborID</code>
	 */
	private final double distance;

	/**
	 * Constructs a new neighbor
	 * @param id
	 * @param neighborID
	 * @param distance
	 */
	public Saving(int neighborID_1, int neighborID_2, double distance){
		this.neighborID_1=neighborID_1;
		this.neighborID_2=neighborID_2;
		this.distance=distance;
	}

	

	public int getNeighborID_1() {
		return neighborID_1;
	}



	public int getNeighborID_2() {
		return neighborID_2;
	}



	public double getDistance() {
		return distance;
	}



	@Override
	public int compareTo(Saving o) {
		if(distance>o.distance)
			return -1;
		return this.distance==o.distance?0:1;
	}
	
	@Override
	public String toString(){
		return "("+this.neighborID_1+","+this.neighborID_2+")\t"+this.distance;
	}
}
