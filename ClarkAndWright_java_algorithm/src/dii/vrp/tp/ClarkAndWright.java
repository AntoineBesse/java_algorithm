package dii.vrp.tp;


import dii.vrp.data.IDemands;
import dii.vrp.data.IDistanceMatrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ClarkAndWright {
    private IDistanceMatrix distances;
    private IDemands demands;
    private double Q;
    private double lambda;

    public  ClarkAndWright(IDistanceMatrix distances, IDemands demands, double Q) {
        this.distances = distances;
        this.demands = demands;
        this.Q = Q;
        
    }

    
    public double getLambda() {
		return lambda;
	}


	public void setLambda(double lambda) {
		this.lambda = lambda;
	}


	private List<Saving> createSavingsList() {
        ArrayList<Saving> savingsList = new ArrayList<>();

        for (int i = 1; i < distances.size(); i++) {
            for (int j = 1; j < distances.size(); j++) {
                if (i != j) {
                    double saving = distances.getDistance(0, i) + distances.getDistance(0, j) - lambda*distances.getDistance(i, j);
                    Saving newNode = new Saving(i, j, saving);

                    if (!savingsList.contains(newNode))
                    savingsList.add(newNode);
                }
            }
        }

        // Sort the savings
        savingsList.sort(Comparator.comparing(Saving::getDistance));
        Collections.reverse(savingsList);

        return savingsList;
    }

    private ISolution run(VRPSolution p_solution, List<Saving> saving) {
        VRPSolution solution = (VRPSolution) p_solution.clone();

        // Iterate through all the saving nodes
        for (Saving node : saving) {
            int mergedRouteIndex = -1;

            for (int x = 0; x < solution.size(); x++) {

                if (mergedRouteIndex != -1)
                    break;
                
                if (solution.getNode(x, solution.size(x) - 2) == node.getNeighborID_1()) {

                    for (int y = 0; y < solution.size(); y++) {
                        if ((solution.getNode(y, 1) == node.getNeighborID_2()) &&
                                (x != y) &&
                                (solution.getLoad(x) + solution.getLoad(y)) <= Q) {

                            // Merge the routes 
                            for (int i = 1; i < (solution.size(y) - 1); i++) {
                                solution.insert(solution.getNode(y, i), x, solution.size(x) - 1);
                            }

                            solution.setCost(x, solution.getCost(x) + solution.getCost(y) - node.getDistance());
                            solution.setLoad(x, solution.getLoad(x) + solution.getLoad(y));
                            mergedRouteIndex = y;
                            break;
                        }
                    }
                }
            }

            if (mergedRouteIndex != -1) {
                solution.remove(mergedRouteIndex);
            }
        }

        // Set total cost of solution
        double total = 0;
        for (int i = 0; i < solution.size(); i++) {
            total += solution.getCost(i);
        }
        solution.setOF(total);
        return solution;
    }


    private VRPSolution prepareInitialSolution() {
        // Master solution (naive version)
        VRPSolution solution = new VRPSolution();

        // Create all the single routes
        for (int i = 1; i < distances.size(); i++) {
            VRPRoute route = new VRPRoute();
            route.add(0);
            route.add(i);
            route.add(0);
            route.setLoad(demands.getDemand(i));
            route.setCost(distances.getDistance(0, i) * 2);
            solution.addRoute(route);
        }
        return solution;
    }

    // Innitialisation + Run 
    public ISolution BessePlociniczakRun() {
    	long debut = System.currentTimeMillis();
        ISolution s = run(prepareInitialSolution(), createSavingsList());
        long fin = System.currentTimeMillis()-debut;
        System.out.println("Time elapsed :" 
        + fin
        + " ms");
       
        return s;
    }


   
}
