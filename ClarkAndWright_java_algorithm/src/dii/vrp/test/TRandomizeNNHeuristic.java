package dii.vrp.test;

import java.util.Random;

import dii.vrp.data.IDemands;
import dii.vrp.data.IDistanceMatrix;
import dii.vrp.data.VRPREPInstanceReader;
import dii.vrp.tp.ISolution;
import dii.vrp.tp.ISplit;
import dii.vrp.tp.NNHeuristic;
import dii.vrp.tp.SFRSHeuristic;
import dii.vrp.tp.Split;

public class TRandomizeNNHeuristic {
	public static void main(String[] args){

		//Parameters
		String file= "./data/christofides-et-al-1979-cmt/CMT01.xml"; //Instance
		
		//Read data from an instance file
		IDistanceMatrix distances=null;
		IDemands demands=null;
		double Q=Double.NaN;
		try(VRPREPInstanceReader parser=new VRPREPInstanceReader(file)){
			distances=parser.getDistanceMatrix();
			demands=parser.getDemands();
			Q=parser.getCapacity("0");
		}

		//Initialize the sequence-first, route-second heuristic
		long debut = System.currentTimeMillis();
		NNHeuristic nn=new NNHeuristic(distances);
		nn.setRandomized(true);
		nn.setRandomGen(new Random(1));
		nn.setRandomizationFactor(3);
		ISolution sol=nn.run();
		long fin = System.currentTimeMillis()-debut;
        System.out.println("Time elapsed :" 
        + fin
        + " ms");
		//Run the heuristic and report the results
		System.out.println(sol);
	}
}
