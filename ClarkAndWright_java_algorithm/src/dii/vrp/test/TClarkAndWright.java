package dii.vrp.test;

import dii.vrp.data.IDemands;
import dii.vrp.data.IDistanceMatrix;
import dii.vrp.data.VRPREPInstanceReader;
import dii.vrp.tp.ClarkAndWright;
import dii.vrp.tp.VRPSolution;


public class TClarkAndWright {
    public static void main(String[] args) {
    	
    	//Read data from an instance file
		IDistanceMatrix distances=null;
		IDemands demands=null;
		double Q=Double.NaN;
		VRPSolution solution;
		VRPSolution old_solution;
		VRPSolution best_solution=null;
        // Instances to test
    	String file= "./data/christofides-et-al-1979-cmt/CMT01.xml"; //Instance

    	try(VRPREPInstanceReader parser=new VRPREPInstanceReader(file)){
			distances=parser.getDistanceMatrix();
			demands=parser.getDemands();
			Q=parser.getCapacity("0");
		}

                System.out.println("Testing instance " + file);
                ClarkAndWright cw = new ClarkAndWright(distances, demands, Q);
                cw.setLambda(1);	               
                solution = (VRPSolution) cw.BessePlociniczakRun();
                System.out.println("First Solution : "+solution); 
                 
               //change lambda  
       /*        old_solution = solution;
               best_solution = solution;
               for(double lambda = 0.5;lambda < 1.5 ;lambda+=0.01) {
            	   
            	   cw.setLambda(lambda);
            	  solution = (VRPSolution) cw.BessePlociniczakRun(); 
            	  
                 if(solution.getOF() < best_solution.getOF()) {
                	 best_solution = solution;
                	 System.out.println("Best Solution : "+best_solution.getOF());
                }
                 
                 old_solution = solution;
                 
                }
               
               System.out.println("Best Solution : "+best_solution);*/
                
            }
           
        
    }

