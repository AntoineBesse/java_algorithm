Using clark an wright :

-> With eclipse import the project ClarkAndWright_java_algorithm
-> Run dii/vrp/test/TClarkAndWright.java

You can test other algorithm with the test package :

TGRASP.java for grasp algorithm;
TNaiveNNHeuristic.java for naive nearest neighbourg algorithm;
TNNHeuristics.java for nearest neighbourg algorithm
TRandomizeNNHeuristic.java for randomized nearest neighbourg algorithm
TSFRSHeuristic.java for SFRS algorithm
TSplit.java for split algorithm

Class Code  in :dii/vrp/tp
Data Code   in :dii/vrp/data

TSP_java_algorithm : TSP algorithm

VRP_java_algorithm  : VRP algorithm

Enjoy ! 