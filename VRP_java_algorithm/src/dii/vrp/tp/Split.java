package dii.vrp.tp;

import dii.vrp.data.IDemands;
import dii.vrp.data.IDistanceMatrix;

/**
 * Implements a basic split procedure. The implementation assumes that the
 * {@link TSPSolution} passed to method {@link #split(TSPSolution)} contains the
 * depot at the begining and end of the route.
 * 
 * @author Jorge E. Mendoza (dev@jorge-mendoza.com)
 * @version %I%, %G%
 * @since Jan 21, 2016
 *
 */
public class Split implements ISplit {
	/**
	 * The distance matrix
	 */
	private final IDistanceMatrix distances;
	/**
	 * The customer demands
	 */
	private final IDemands demands;
	/**
	 * The vehicle's capacity
	 */
	private final double Q;

	/**
	 * 
	 * @param distances
	 * @param demands
	 * @param Q
	 */
	public Split(IDistanceMatrix distances, IDemands demands, double Q) {
		this.distances = distances;
		this.demands = demands;
		this.Q = Q;
	}

	@Override
	public ISolution split(TSPSolution r) {

		// TODO EXO2: We will implement the algorithm together

		int Vo = 0;
		int n = r.size() - 2;
		double vi[] = new double[n + 1];
		int pi[] = new int[n + 1];
		double load = 0;
		double cost = 0;
		int j = 0;
		double W = Q;
		vi[0] = 0;
		// double L = 0;

		for (int k = 1; k <= n; k++) {

			vi[k] = Double.POSITIVE_INFINITY;
		}

		for (int i = 1; i <= n; i++) {
			load = 0;
			cost = 0;
			j = i;

			do {
				load = load + demands.getDemand(r.get(j));

				if (i == j) {
					cost = distances.getDistance(0, r.get(j)) + distances.getDistance(r.get(j), 0);
				} else {
					cost = cost - distances.getDistance(r.get(j - 1), 0) + distances.getDistance(r.get(j - 1), r.get(j))
							+ distances.getDistance(r.get(j), 0);
				}
				if (load <= W) {

					if (vi[i - 1] + cost < vi[j]) {

						vi[j] = vi[i - 1] + cost;
						pi[j] = i - 1;
					}

					j = j + 1;

				}
			} while (!((j > n) || (load > this.Q)));
		}

		return extractRoutes(pi, vi, r);
	}

	/**
	 * Extracts the routes from the labels, builds a solution, and evaluates the
	 * solution
	 * 
	 * @return a solution with the routes in the optimal partition of the TSP tour
	 */
	private VRPSolution extractRoutes(int[] P, double[] V, TSPSolution tsp) {

		// TODO EXO2: implement this method
		
		VRPSolution soluce = new VRPSolution();
		soluce.setOF(0);
		
		int t = 0;
		int j = tsp.size()-2;
		int i = 0;
		
		do {
			
			t = t + 1;
			i = P[j];
			VRPRoute Route = new VRPRoute();
			Route.setCost(0);
			Route.setLoad(0);
			
			
			Route.add(tsp.get(0));
			
			for (int k = i + 1; k <= j; k++) {
				Route.add(tsp.get(k));
				Route.setCost(Route.getCost()+distances.getDistance(Route.get(Route.size()-2), k));
				Route.setLoad(Route.getLoad()+demands.getDemand(tsp.get(k)));
			}
			j = i;
			Route.add(tsp.get(0));
			Route.setCost(Route.getCost()+distances.getDistance(Route.get(Route.size()-2), 0));
			soluce.addRoute(Route);
			
			soluce.setOF(soluce.getOF()+Route.getCost());
			
			
		} while (i != 0);

		return soluce;

	}

}
