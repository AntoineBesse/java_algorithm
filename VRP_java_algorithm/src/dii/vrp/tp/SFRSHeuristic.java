package dii.vrp.tp;

public class SFRSHeuristic implements IOptimizationAlgorithm {


	ITSPHeuristic h;
	ISplit s;
	
	 public SFRSHeuristic(final ITSPHeuristic h, final ISplit s) {
		 this.h=h;
		 this.s=s;
		 
	 }
	 
		@Override
		public ISolution run() {
			
			
			return s.split(h.run());
		}
	 
	 
}
